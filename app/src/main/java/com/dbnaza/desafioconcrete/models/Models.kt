package com.dbnaza.desafioconcrete.models

import com.google.gson.annotations.SerializedName

class Models {
    data class ProjectsResponse(
            @SerializedName("total_count") val totalCount: Int,
            @SerializedName("items") val projects: MutableList<Project>
    )

    data class Project(
            @SerializedName("id") val id: Int,
            @SerializedName("name") val name: String?,
            @SerializedName("owner") val owner: Owner?,
            @SerializedName("description") val description: String?,
            @SerializedName("stargazers_count") val stargazersCount: Int,
            @SerializedName("forks_count") val forksCount: Int
    )

    data class Owner(
            @SerializedName("login") val login: String?,
            @SerializedName("id") val id: Int,
            @SerializedName("avatar_url") val avatarUrl: String?
    )

    data class PullRequest(
            @SerializedName("id") val id: Int,
            @SerializedName("title") val title: String?,
            @SerializedName("user") val owner: Owner?,
            @SerializedName("body") val body: String?,
            @SerializedName("updated_at") val updatedAt: String?
    )
}