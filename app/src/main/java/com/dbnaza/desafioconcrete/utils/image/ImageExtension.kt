package com.dbnaza.desafioconcrete.utils.image

import android.databinding.BindingAdapter
import android.widget.ImageView

@BindingAdapter("imageUrl")
fun ImageView.imageUrl(url: String) {
    CachedPicasso.load(url).fit().centerCrop().into(this)
}