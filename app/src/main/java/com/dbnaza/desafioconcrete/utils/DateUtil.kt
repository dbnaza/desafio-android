package com.dbnaza.desafioconcrete.utils

import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat


object DateUtil {
    fun formatDate(time: String?): String? {
        if (time.isNullOrEmpty().not()) {
            val date = DateTime(time)
            val dateFormat = DateTimeFormat.forPattern("HH:mm dd/MM/yyyy")
            return dateFormat.print(date)
        }
        return time
    }
}