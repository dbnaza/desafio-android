package com.dbnaza.desafioconcrete.utils.image

import android.content.Context
import com.dbnaza.desafioconcrete.application.AppApplication
import com.dbnaza.desafioconcrete.network.OkHttpProvider
import com.squareup.picasso.OkHttp3Downloader
import com.squareup.picasso.Picasso

class CachedPicasso private constructor() {
    companion object {
        private val picasso: Picasso by lazy {
            createPicasso(AppApplication.instance)
        }

        @JvmStatic
        fun load(path: String?) = CachedPicassoRequest(path, picasso)

        @JvmStatic
        private fun createPicasso(context: Context) = Picasso.Builder(context)
                .downloader(OkHttp3Downloader(createOkHttpClient(context)))
                .build()

        @JvmStatic
        private fun createOkHttpClient(context: Context) = OkHttpProvider(context)
                .build()
    }
}