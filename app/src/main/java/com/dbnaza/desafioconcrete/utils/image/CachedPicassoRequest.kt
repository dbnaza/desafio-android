package com.dbnaza.desafioconcrete.utils.image

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.widget.ImageView
import com.squareup.picasso.Callback
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import com.squareup.picasso.RequestCreator
import java.io.IOException
import java.lang.Exception

class CachedPicassoRequest private constructor(val picasso: Picasso) {

    private var requestCreator: RequestCreator? = null
    private var requestCreatorForceCache: RequestCreator? = null

    private var placeholderResId: Int = 0
    private var placeholderDrawable: Drawable? = null

    private var errorResId: Int = 0
    private var errorDrawable: Drawable? = null

    constructor(path: String?, picasso: Picasso) : this(picasso) {
        if ((path != null) && (path.isNotBlank())) {
            requestCreator = picasso.load(path)
            requestCreatorForceCache = picasso.load(path)
                    .networkPolicy(NetworkPolicy.OFFLINE)
        }
    }

    @Throws(IOException::class)
    fun get(): Bitmap? = requestCreator?.get()

    @JvmOverloads
    fun into(target: ImageView, callback: Callback? = null) {
        if (requestCreatorForceCache != null) {
            setPlaceholder(requestCreatorForceCache)
            requestCreatorForceCache?.into(target, object : Callback {
                override fun onSuccess() {
                    placeholderDrawable = target.drawable
                    placeholderResId = 0
                    setPlaceholder(requestCreator)

                    requestCreator?.into(target, object : Callback {
                        override fun onSuccess() {
                            callback?.onSuccess()
                        }

                        override fun onError(e: Exception?) {
                        }
                    })
                }

                override fun onError(e: Exception?) {
                    setPlaceholder(requestCreator)
                    requestCreator?.into(target, object : Callback {
                        override fun onSuccess() {
                            callback?.onSuccess()
                        }

                        override fun onError(e: Exception?) {
                            showErrorImage(target)
                            callback?.onError(e)
                        }
                    })
                }
            })
        } else {
            setPlaceholder(requestCreator)
            setError(requestCreator)
            requestCreator?.into(target)
        }
    }

    private fun setError(requestCreator: RequestCreator?) {
        if (errorResId != 0) {
            requestCreator?.error(errorResId)
        } else if (errorDrawable != null) {
            requestCreator?.error(errorDrawable!!)
        }
    }

    private fun setPlaceholder(requestCreator: RequestCreator?) {
        if (placeholderResId != 0) {
            requestCreator?.placeholder(placeholderResId)
        } else if (placeholderDrawable != null) {
            requestCreator?.placeholder(placeholderDrawable!!)
        }
    }

    private fun showErrorImage(target: ImageView) {
        if (errorResId != 0) {
            target.setImageResource(errorResId)
        } else if (errorDrawable != null) {
            target.setImageDrawable(errorDrawable)
        }
    }

    fun fit(): CachedPicassoRequest {
        requestCreator?.fit()
        requestCreatorForceCache?.fit()
        return this
    }

    fun centerCrop(): CachedPicassoRequest {
        requestCreator?.centerCrop()
        requestCreatorForceCache?.centerCrop()
        return this
    }
}