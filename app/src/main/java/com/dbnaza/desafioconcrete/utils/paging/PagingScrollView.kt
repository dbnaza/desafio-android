package com.dbnaza.desafioconcrete.utils.paging

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView

class PagingScrollView(private val pageSize: Int, page: Int,
                       private val onPaging: () -> Unit) : RecyclerView.OnScrollListener() {
    var page = 0
    var totalSize: Int = 0
    var isLoading = false

    private val isLastPage: Boolean
        get() = expectedNumOfItems >= totalSize

    private val expectedNumOfItems: Int
        get() = pageSize * (page + 1)

    init {
        this.page = page
    }

    override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        paginateOnScroll(recyclerView!!)
    }

    private fun paginateOnScroll(recyclerView: RecyclerView) {
        val layoutManager = recyclerView.layoutManager

        val visibleItemCount = layoutManager.childCount
        val totalItemCount = layoutManager.itemCount
        val firstVisibleItemPosition = (layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()

        if (!isLoading && !isLastPage) {
            if (visibleItemCount + firstVisibleItemPosition >= totalItemCount
                    && firstVisibleItemPosition >= 0
                    && totalItemCount >= expectedNumOfItems) {
                onPaging()
            }
        }
    }
}
