package com.dbnaza.desafioconcrete.utils.paging

import android.databinding.BindingAdapter
import android.support.v7.widget.RecyclerView

@BindingAdapter("pagination")
fun RecyclerView.pagination(pagingScrollView: PagingScrollView) {
    this.addOnScrollListener(pagingScrollView)
}