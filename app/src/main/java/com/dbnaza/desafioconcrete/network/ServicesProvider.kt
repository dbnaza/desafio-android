package com.dbnaza.desafioconcrete.network

import android.content.Context

class ServicesProvider {
    companion object {
        fun getGitHubService(applicationContext: Context, forceCache: Boolean = false): GitHubService {
            return GitHubService(RetrofitBuilder(applicationContext)
                    .forceCache(forceCache)
                    .build()
                    .create(GitHubRest::class.java))
        }
    }
}