package com.dbnaza.desafioconcrete.network

import android.content.Context
import com.dbnaza.desafioconcrete.network.GitHubRest.Companion.URL
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitBuilder(private val applicationContext: Context) {
    private var forceCache = false

    fun forceCache(forceCache: Boolean): RetrofitBuilder {
        this.forceCache = forceCache
        return this
    }

    fun build(): Retrofit {
        return Retrofit.Builder()
                .baseUrl(URL)
                .client(OkHttpProvider(applicationContext)
                        .forceCache(forceCache)
                        .build())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }
}
