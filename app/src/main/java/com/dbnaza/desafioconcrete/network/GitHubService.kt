package com.dbnaza.desafioconcrete.network

import com.dbnaza.desafioconcrete.models.Models.PullRequest
import com.dbnaza.desafioconcrete.models.Models.ProjectsResponse
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class GitHubService(private val gitHubRest: GitHubRest) {
    fun projectsResponse(perPage: Int, page: Int): Single<ProjectsResponse> {
        return gitHubRest.repositories(GitHubRest.QUERY, GitHubRest.SORT, perPage, page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun pullRequests(ownerName: String, repositoryName: String): Single<List<PullRequest>> {
        return gitHubRest.pullRequests(ownerName, repositoryName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}