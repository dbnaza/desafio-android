package com.dbnaza.desafioconcrete.network

import android.content.Context
import com.dbnaza.desafioconcrete.utils.NetworkUtil.hasNetwork
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor

class OkHttpProvider(private val context: Context) {
    private var forceCache = false

    fun forceCache(forceCache: Boolean): OkHttpProvider {
        this.forceCache = forceCache
        return this
    }

    fun build(): OkHttpClient {
        val builder = OkHttpClient.Builder()

        if (forceCache) {
            val networkInterceptor = Interceptor { chain ->
                val originalRequest = chain.request()
                val cacheHeaderValue = if (hasNetwork(context))
                    "public, max-age=$CACHE_STALE"
                else
                    "public, only-if-cached, max-stale=$CACHE_STALE"
                val request = originalRequest.newBuilder().build()
                val response = chain.proceed(request)
                response.newBuilder()
                        .removeHeader("Pragma")
                        .removeHeader("Cache-Control")
                        .header("Cache-Control", cacheHeaderValue)
                        .build()
            }

            builder.addInterceptor(networkInterceptor)
            builder.networkInterceptors().add(networkInterceptor)
        }

        return builder
                .cache(Cache(context.cacheDir, CACHE_SIZE_10_MEGA_BYTES.toLong()))
                .build()
    }

    companion object {
        private const val CACHE_SIZE_10_MEGA_BYTES = 10 * 1024 * 1024
        private const val CACHE_STALE = 1 * 60 * 60 * 24
    }
}
