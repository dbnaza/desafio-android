package com.dbnaza.desafioconcrete.network

import com.dbnaza.desafioconcrete.models.Models.PullRequest
import com.dbnaza.desafioconcrete.models.Models.ProjectsResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GitHubRest {
    companion object {
        const val URL = "https://api.github.com/"

        const val QUERY = "language:Java"
        const val SORT = "stars"
        const val OWNER_NAME = "owner_name"
        const val REPOSITORY_NAME = "repository_name"
    }

    @GET("/search/repositories")
    fun repositories(@Query("q") query: String = QUERY,
                     @Query("sort") sort: String = SORT,
                     @Query("per_page") perPage: Int,
                     @Query("page") page: Int): Single<ProjectsResponse>

    @GET("/repos/{$OWNER_NAME}/{$REPOSITORY_NAME}/pulls")
    fun pullRequests(@Path(OWNER_NAME) owner: String,
                     @Path(REPOSITORY_NAME) repository: String): Single<List<PullRequest>>
}
