package com.dbnaza.desafioconcrete.application

import android.app.Application
import net.danlew.android.joda.JodaTimeAndroid

class AppApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        instance = this
        JodaTimeAndroid.init(this)
    }

    companion object {
        lateinit var instance: AppApplication
            private set
    }
}