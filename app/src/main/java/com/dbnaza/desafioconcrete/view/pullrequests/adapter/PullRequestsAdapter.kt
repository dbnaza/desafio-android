package com.dbnaza.desafioconcrete.view.pullrequests.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.dbnaza.desafioconcrete.R
import com.dbnaza.desafioconcrete.databinding.RowPullRequestsBinding
import com.dbnaza.desafioconcrete.models.Models.PullRequest
import com.dbnaza.desafioconcrete.viewmodel.RowPullRequestViewModel

class PullRequestsAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var list: MutableList<PullRequest> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding: RowPullRequestsBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context),
                R.layout.row_pull_requests, parent, false)

       return PullRequestViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        getItem(position)?.let { (holder as PullRequestViewHolder).bindView(it) }
    }

    override fun getItemId(position: Int): Long {
        val post = getItem(position)
        return post?.id?.toLong() ?: System.currentTimeMillis()
    }

    fun update(listResult: MutableList<PullRequest>) {
        this.list = listResult
        notifyDataSetChanged()
    }

    private fun getItem(position: Int): PullRequest? {
        return if (position < list.size) {
            list[position]
        } else {
            null
        }
    }

    class PullRequestViewHolder(private val binding: RowPullRequestsBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bindView(PullRequest: PullRequest) {
            binding.viewModel = RowPullRequestViewModel(PullRequest)
            binding.executePendingBindings()
        }
    }
}