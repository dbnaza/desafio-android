package com.dbnaza.desafioconcrete.view.repositories.adapter

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.dbnaza.desafioconcrete.R
import com.dbnaza.desafioconcrete.databinding.RowLoadingBinding
import com.dbnaza.desafioconcrete.databinding.RowProjectBinding
import com.dbnaza.desafioconcrete.models.Models.Project
import com.dbnaza.desafioconcrete.viewmodel.RowProjectViewModel

class ProjectsAdapter(private val itemClick: (project: Project) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var list: MutableList<Project> = mutableListOf()
    private var isPaging = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            VIEW_TYPE_ITEM -> {
                val binding: RowProjectBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context),
                        R.layout.row_project, parent, false)
                ProjectViewHolder(binding, itemClick)
            }
            else -> {
                val binding: RowLoadingBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context),
                        R.layout.row_loading, parent, false)
                LoadingHolder(binding)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            isPagingItem(position) -> VIEW_TYPE_PAGING
            else -> VIEW_TYPE_ITEM
        }
    }

    override fun getItemCount(): Int {
        return list.size + if (isPaging) 1 else 0
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        getItem(position)?.let { (holder as ProjectViewHolder).bindView(it) }
    }

    override fun getItemId(position: Int): Long {
        val post = getItem(position)
        return post?.id?.toLong() ?: System.currentTimeMillis()
    }

    fun update(listResult: MutableList<Project>) {
        this.list = listResult
        notifyDataSetChanged()
    }

    private fun getItem(position: Int): Project? {
        return if (position < list.size) {
            list[position]
        } else {
            null
        }
    }

    private fun isPagingItem(position: Int): Boolean = isPaging && position >= list.size

    fun setPaging(isPaging : Boolean) {
        this.isPaging = isPaging
        notifyDataSetChanged()
    }

    companion object {
        private const val VIEW_TYPE_ITEM = 0
        private const val VIEW_TYPE_PAGING = 1
    }

    class ProjectViewHolder(private val binding: RowProjectBinding,
                            private val itemClick: (project: Project) -> Unit) : RecyclerView.ViewHolder(binding.root) {
        fun bindView(project: Project) {
            binding.viewModel = RowProjectViewModel(project, itemClick)
            binding.executePendingBindings()
        }
    }

    class LoadingHolder(binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root)
}