package com.dbnaza.desafioconcrete.view.pullrequests

import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.databinding.Observable
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.dbnaza.desafioconcrete.R
import com.dbnaza.desafioconcrete.databinding.ActivityProjectsBinding
import com.dbnaza.desafioconcrete.databinding.ActivityPullRequestsBinding
import com.dbnaza.desafioconcrete.network.ServicesProvider
import com.dbnaza.desafioconcrete.utils.addOnPropertyChanged
import com.dbnaza.desafioconcrete.view.pullrequests.adapter.PullRequestsAdapter
import com.dbnaza.desafioconcrete.view.repositories.adapter.ProjectsAdapter
import com.dbnaza.desafioconcrete.viewmodel.PullRequestsViewModel


class PullRequestsActivity : AppCompatActivity() {

    private var viewModel: PullRequestsViewModel? = null
    private var listChangedCallback: Observable.OnPropertyChangedCallback? = null

    private val adapter = PullRequestsAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityPullRequestsBinding = DataBindingUtil
                .setContentView(this, R.layout.activity_pull_requests)

        val ownerName = intent.getStringExtra(OWNER_NAME)
        val projectName = intent.getStringExtra(PROJECT_NAME)

        title = "$ownerName/$projectName"

        viewModel = PullRequestsViewModel(ownerName, projectName, ServicesProvider.getGitHubService(applicationContext))
        binding.viewModel = viewModel

        observeCallbacks()

        adapter.setHasStableIds(true)
        binding.list.adapter = adapter

        viewModel?.loadPullRequests()
    }

    private fun observeCallbacks() {
        listChangedCallback =
                viewModel?.listResult?.addOnPropertyChanged { observable ->
                    observable.get().let { list ->
                        list?.toMutableList()?.let { it -> adapter.update(it) }
                    }
                }

        listChangedCallback?.let { viewModel?.listResult?.addOnPropertyChangedCallback(it) }
    }

    override fun onDestroy() {
        stopObservingCallbacks()
        viewModel?.destroy()

        super.onDestroy()
    }

    private fun stopObservingCallbacks() {
        listChangedCallback?.let { viewModel?.listResult?.removeOnPropertyChangedCallback(it) }
    }

    companion object {
        private const val OWNER_NAME = "OWNER_NAME"
        private const val PROJECT_NAME = "PROJECT_NAME"

        @JvmStatic
        fun startActivity(context: Context, ownerName: String, projectName: String) {
            val intent = Intent(context, PullRequestsActivity::class.java)
            intent.putExtra(OWNER_NAME, ownerName)
            intent.putExtra(PROJECT_NAME, projectName)
            context.startActivity(intent)
        }
    }
}
