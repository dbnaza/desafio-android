package com.dbnaza.desafioconcrete.view.repositories

import android.databinding.DataBindingUtil
import android.databinding.Observable
import android.databinding.ObservableBoolean
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.dbnaza.desafioconcrete.R
import com.dbnaza.desafioconcrete.databinding.ActivityProjectsBinding
import com.dbnaza.desafioconcrete.network.ServicesProvider
import com.dbnaza.desafioconcrete.utils.addOnPropertyChanged
import com.dbnaza.desafioconcrete.view.pullrequests.PullRequestsActivity
import com.dbnaza.desafioconcrete.view.repositories.adapter.ProjectsAdapter
import com.dbnaza.desafioconcrete.viewmodel.ProjectsViewModel


class ProjectsActivity : AppCompatActivity() {

    private var viewModel: ProjectsViewModel? = null
    private var listChangedCallback: Observable.OnPropertyChangedCallback? = null

    private val adapter = ProjectsAdapter {
        val ownerName = it.owner?.login
        val projectName = it.name
        if (ownerName.isNullOrEmpty().not() && projectName.isNullOrEmpty().not()) {
            PullRequestsActivity.startActivity(this, ownerName!!, projectName!!)
        }
    }

    private val onPagingPropertyChangedCallback = object : Observable.OnPropertyChangedCallback() {
        override fun onPropertyChanged(observable: Observable?, intValue: Int) {
            val isPaging = (observable as ObservableBoolean).get()
            adapter.setPaging(isPaging)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityProjectsBinding = DataBindingUtil
                .setContentView(this, R.layout.activity_projects)

        title = getString(R.string.projects_title)

        viewModel = ProjectsViewModel(ServicesProvider.getGitHubService(applicationContext))
        binding.viewModel = viewModel

        observeCallbacks()

        adapter.setHasStableIds(true)
        binding.list.adapter = adapter

        viewModel?.loadProjects(0)
    }

    private fun observeCallbacks() {
        listChangedCallback =
                viewModel?.listResult?.addOnPropertyChanged {
                    it.get().let {
                        adapter.update(it?.projects!!)
                    }
                }

        listChangedCallback?.let { viewModel?.listResult?.addOnPropertyChangedCallback(it) }

        viewModel?.isPaging?.addOnPropertyChangedCallback(onPagingPropertyChangedCallback)
    }

    override fun onDestroy() {
        stopObservingCallbacks()
        viewModel?.destroy()

        super.onDestroy()
    }

    private fun stopObservingCallbacks() {
        listChangedCallback?.let { viewModel?.listResult?.removeOnPropertyChangedCallback(it) }
        viewModel?.isPaging?.removeOnPropertyChangedCallback(onPagingPropertyChangedCallback)
    }
}
