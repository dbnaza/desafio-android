package com.dbnaza.desafioconcrete.viewmodel

import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import com.dbnaza.desafioconcrete.models.Models.ProjectsResponse
import com.dbnaza.desafioconcrete.network.GitHubService
import com.dbnaza.desafioconcrete.utils.paging.PagingScrollView

class ProjectsViewModel(private val gitHubService: GitHubService) : BaseViewModel() {
    val listResult: ObservableField<ProjectsResponse> = ObservableField()
    val isPaging = ObservableBoolean(false)
    private var isLastPage = false
    private var currentPage = 0

    var pagingScrollView = PagingScrollView(PER_PAGE, currentPage) {
        currentPage++
        loadProjects(currentPage)
    }

    fun loadProjects(page: Int) {
        currentPage = page
        if (currentPage == 0 && hasNoProjectsListed()) {
            setProgressState()
        }

        if (!isLastPage) {
            isPaging.set(currentPage != 0)
            pagingScrollView.isLoading = true

            disposable = gitHubService
                    .projectsResponse(PER_PAGE, currentPage)
                    .subscribe(this::getProjectsSuccess, this::getProjectsError)
        }
    }

    private fun getProjectsSuccess(result: ProjectsResponse) {
        if (currentPage == 0 && result.projects.isEmpty()) {
            setEmptyState()
        } else {
            pagingScrollView.page = currentPage
            pagingScrollView.totalSize = result.totalCount
            pagingScrollView.isLoading = false

            val size = listResult.get()?.projects?.size ?: 0

            if (listResult.get() == null) {
                listResult.set(result)
            } else {
                if (size > 0 && (currentPage == 0)) {
                    listResult.get()?.projects?.clear()
                }
                listResult.get()?.projects?.addAll(result.projects)
                listResult.notifyChange()
            }

            setListState()

            isPaging.set(false)
            isLastPage = size == result.totalCount
        }
    }

    private fun getProjectsError(throwable: Throwable) {
        pagingScrollView.isLoading = false

        if (currentPage == 0 && hasNoProjectsListed()) {
            setReloadState()
        } else {
            setListState()
            currentPage--
        }

        isPaging.set(false)
    }

    private fun hasNoProjectsListed() = (listResult.get()?.projects?.size ?: 0) == 0

    override fun onClickReload() {
        loadProjects(currentPage)
    }

    companion object {
        const val PER_PAGE = 30
    }
}