package com.dbnaza.desafioconcrete.viewmodel

import com.dbnaza.desafioconcrete.models.Models.Project


class RowProjectViewModel(private val project: Project,
                          private val itemClick: (project: Project) -> Unit) {
    fun getOwnerName() = project.owner?.login ?: ""

    fun getOwnerPhoto() = project.owner?.avatarUrl ?: ""

    fun getProjectName() = project.name ?: ""

    fun getProjectDescription() = project.description ?: ""

    fun getProjectForksCount() = project.forksCount.toString()

    fun getProjectStarsCount() = project.stargazersCount.toString()

    fun onItemClick() {
        itemClick(project)
    }
}