package com.dbnaza.desafioconcrete.viewmodel

import android.databinding.BaseObservable
import android.databinding.BindingAdapter
import android.databinding.ObservableInt
import android.widget.ViewFlipper
import io.reactivex.disposables.Disposable

abstract class BaseViewModel : BaseObservable(){
    val viewFlipperActiveChild = ObservableInt(CHILD_PROGRESS)
    protected var disposable: Disposable? = null

    protected fun setProgressState() {
        viewFlipperActiveChild.set(CHILD_PROGRESS)
    }

    protected fun setListState() {
        viewFlipperActiveChild.set(CHILD_LIST)
    }

    protected fun setReloadState() {
        viewFlipperActiveChild.set(CHILD_RELOAD)
    }

    protected fun setEmptyState() {
        viewFlipperActiveChild.set(CHILD_EMPTY)
    }

    fun destroy() {
        if (disposable?.isDisposed?.not() == true) {
            disposable?.dispose()
        }
    }

    abstract fun onClickReload()

    companion object {
        const val CHILD_PROGRESS = 0
        const val CHILD_LIST = 1
        const val CHILD_RELOAD = 2
        const val CHILD_EMPTY = 3

        @JvmStatic
        @BindingAdapter("viewFlipperChild")
        fun setViewFlipperChild(viewFlipper: ViewFlipper, child: Int) {
            viewFlipper.displayedChild = child
        }
    }
}