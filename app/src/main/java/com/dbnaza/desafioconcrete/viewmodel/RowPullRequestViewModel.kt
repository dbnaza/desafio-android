package com.dbnaza.desafioconcrete.viewmodel

import com.dbnaza.desafioconcrete.models.Models.PullRequest
import com.dbnaza.desafioconcrete.utils.DateUtil.formatDate


class RowPullRequestViewModel(private val pullRequest: PullRequest) {
    fun getOwnerName() = pullRequest.owner?.login ?: ""

    fun getOwnerPhoto() = pullRequest.owner?.avatarUrl ?: ""

    fun getPullRequestName() = pullRequest.title ?: ""

    fun getPullRequestBody() = pullRequest.body ?: ""

    fun getPullRequestDate() = formatDate(pullRequest.updatedAt) ?: ""
}