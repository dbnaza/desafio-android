package com.dbnaza.desafioconcrete.viewmodel

import android.databinding.ObservableField
import com.dbnaza.desafioconcrete.models.Models.PullRequest
import com.dbnaza.desafioconcrete.network.GitHubService

class PullRequestsViewModel(private val ownerName: String,
                            private val projectName: String,
                            private val gitHubService: GitHubService) : BaseViewModel() {
    val listResult: ObservableField<List<PullRequest>> = ObservableField()

    fun loadPullRequests() {
        disposable = gitHubService
                .pullRequests(ownerName, projectName)
                .subscribe(this::getProjectsSuccess, this::getProjectsError)
    }

    private fun getProjectsSuccess(result: List<PullRequest>) {
        if (result.isEmpty()) {
            setEmptyState()
        } else {
            listResult.set(result)
            setListState()
        }
    }

    private fun getProjectsError(throwable: Throwable) {
        if (hasNoProjectsListed()) {
            setReloadState()
        } else {
            setListState()
        }
    }

    private fun hasNoProjectsListed() = (listResult.get()?.size ?: 0) == 0

    override fun onClickReload() {
        loadPullRequests()
    }
}