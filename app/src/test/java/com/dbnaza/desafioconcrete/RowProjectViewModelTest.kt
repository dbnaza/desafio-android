package com.dbnaza.desafioconcrete

import com.dbnaza.desafioconcrete.models.Models.Owner
import com.dbnaza.desafioconcrete.models.Models.Project
import com.dbnaza.desafioconcrete.viewmodel.RowProjectViewModel
import io.mockk.mockk
import io.mockk.verify
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class RowProjectViewModelTest {

    private lateinit var owner: Owner
    private lateinit var project: Project
    private lateinit var rowProjectViewModel: RowProjectViewModel
    private val itemClick = mockk<(Project) -> Unit>(relaxed = true)

    @Before
    fun setup() {
        owner = Owner(OWNER_NAME, OWNER_ID, OWNER_AVATAR)
        project = Project(PROJECT_ID, PROJECT_NAME, owner, PROJECT_DESCRIPTION, PROJECT_STARS, PROJECT_FORKS)

        rowProjectViewModel = RowProjectViewModel(project, itemClick)
    }

    @Test
    fun whenReceiveProjectShouldShowOwnerName() {
        assertEquals(OWNER_NAME, rowProjectViewModel.getOwnerName())
    }

    @Test
    fun whenReceiveProjectShouldShowOwnerPhoto() {
        assertEquals(OWNER_AVATAR, rowProjectViewModel.getOwnerPhoto())
    }

    @Test
    fun whenReceiveProjectShouldShowProjectName() {
        assertEquals(PROJECT_NAME, rowProjectViewModel.getProjectName())
    }

    @Test
    fun whenReceiveProjectShouldShowProjectDescription() {
        assertEquals(PROJECT_DESCRIPTION, rowProjectViewModel.getProjectDescription())
    }

    @Test
    fun whenReceiveProjectShouldShowProjectStarsCount() {
        assertEquals(PROJECT_STARS.toString(), rowProjectViewModel.getProjectStarsCount())
    }

    @Test
    fun whenReceiveProjectShouldShowProjectForksCount() {
        assertEquals(PROJECT_FORKS.toString(), rowProjectViewModel.getProjectForksCount())
    }

    @Test
    fun whenClickOwnerItemListShouldOpenProfile() {
        rowProjectViewModel.onItemClick()
        verify { itemClick.invoke(project) }
    }

    companion object {
        const val PROJECT_ID = 1
        const val PROJECT_NAME = "Desafio Android"
        const val PROJECT_DESCRIPTION = "Lorem Ipsum Dolor"
        const val PROJECT_STARS = 100
        const val PROJECT_FORKS = 10

        const val OWNER_ID = 1
        const val OWNER_NAME = "Daniel Nazareth"
        const val OWNER_AVATAR = "picture"
    }
}