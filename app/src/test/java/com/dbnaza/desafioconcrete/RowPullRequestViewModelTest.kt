package com.dbnaza.desafioconcrete

import com.dbnaza.desafioconcrete.models.Models.Owner
import com.dbnaza.desafioconcrete.models.Models.PullRequest
import com.dbnaza.desafioconcrete.utils.DateUtil.formatDate
import com.dbnaza.desafioconcrete.viewmodel.RowPullRequestViewModel
import io.mockk.mockk
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class RowPullRequestViewModelTest {

    private lateinit var owner: Owner
    private lateinit var pullRequest: PullRequest
    private lateinit var rowPullRequestViewModel: RowPullRequestViewModel

    @Before
    fun setup() {
        owner = Owner(OWNER_NAME, OWNER_ID, OWNER_AVATAR)
        pullRequest = PullRequest(PULL_REQUEST_ID, PULL_REQUEST_TITLE, owner, PULL_REQUEST_BODY, PULL_REQUEST_DATE)

        rowPullRequestViewModel = RowPullRequestViewModel(pullRequest)
    }

    @Test
    fun whenReceivePullRequestShouldShowOwnerName() {
        assertEquals(OWNER_NAME, rowPullRequestViewModel.getOwnerName())
    }

    @Test
    fun whenReceivePullRequestShouldShowOwnerPhoto() {
        assertEquals(OWNER_AVATAR, rowPullRequestViewModel.getOwnerPhoto())
    }

    @Test
    fun whenReceivePullRequestShouldShowPullRequestTitle() {
        assertEquals(PULL_REQUEST_TITLE, rowPullRequestViewModel.getPullRequestName())
    }

    @Test
    fun whenReceivePullRequestShouldShowPullRequestBody() {
        assertEquals(PULL_REQUEST_BODY, rowPullRequestViewModel.getPullRequestBody())
    }

    @Test
    fun whenReceivePullRequestShouldShowPullRequestDate() {
        assertEquals(formatDate(PULL_REQUEST_DATE), rowPullRequestViewModel.getPullRequestDate())
    }

    companion object {
        const val PULL_REQUEST_ID = 1
        const val PULL_REQUEST_TITLE = "Code changes"
        const val PULL_REQUEST_BODY = "Lorem Ipsum Dolor"
        const val PULL_REQUEST_DATE = "2018-07-30T06:56:30Z"

        const val OWNER_ID = 1
        const val OWNER_NAME = "Daniel Nazareth"
        const val OWNER_AVATAR = "picture"
    }
}