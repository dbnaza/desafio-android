package com.dbnaza.desafioconcrete

import com.dbnaza.desafioconcrete.models.Models.*
import com.dbnaza.desafioconcrete.network.GitHubService
import com.dbnaza.desafioconcrete.rx.RxSchedulerRule
import com.dbnaza.desafioconcrete.viewmodel.BaseViewModel.Companion.CHILD_LIST
import com.dbnaza.desafioconcrete.viewmodel.BaseViewModel.Companion.CHILD_RELOAD
import com.dbnaza.desafioconcrete.viewmodel.ProjectsViewModel
import io.mockk.every
import io.mockk.mockk
import io.reactivex.Single
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class ProjectViewModelTest {

    private lateinit var owner: Owner
    private lateinit var project: Project
    private lateinit var gitHubService: GitHubService
    private lateinit var viewModel: ProjectsViewModel

    @get:Rule
    var rule = RxSchedulerRule()

    @Before
    fun setup() {
        owner = Owner(OWNER_NAME, OWNER_ID, OWNER_AVATAR)
        project = Project(PROJECT_ID, PROJECT_NAME, owner, PROJECT_DESCRIPTION, PROJECT_STARS, PROJECT_FORKS)

        gitHubService = mockk(relaxed = true)
        viewModel = ProjectsViewModel(gitHubService)
    }

    @Test
    fun whenServiceReturnSuccessShouldShowList() {
        every { gitHubService.projectsResponse(any(), any()) } returns Single.just(createProjectsResponse(mutableListOf(project)))

        viewModel.loadProjects(0)

        assertEquals(mutableListOf(project), viewModel.listResult.get()?.projects)
    }

    @Test
    fun whenServiceReturnWithErrorAndIsFirstPageShouldShowReloadButton() {
        every { gitHubService.projectsResponse(any(), any()) } returns Single.error(Exception())

        viewModel.loadProjects(0)

        assertEquals(CHILD_RELOAD, viewModel.viewFlipperActiveChild.get())
    }

    @Test
    fun whenServiceReturnWithErrorAndIsNotFirstPageShouldNotShowReloadButton() {
        every { gitHubService.projectsResponse(any(), any()) } returns Single.error(Exception())

        viewModel.loadProjects(1)

        assertEquals(CHILD_LIST, viewModel.viewFlipperActiveChild.get())
    }

    @Test
    fun whenServiceReturnWithErrorAndListIsEmptyShouldShowReloadButton() {
        every { gitHubService.projectsResponse(any(), any()) } returns Single.error(Exception())

        viewModel.loadProjects(0)

        assertEquals(CHILD_RELOAD, viewModel.viewFlipperActiveChild.get())
    }

    @Test
    fun whenServiceReturnWithErrorAndListIsNowEmptyShouldShowNotReloadButton() {
        every { gitHubService.projectsResponse(any(), any()) } returns Single.error(Exception())

        viewModel.listResult.set(createProjectsResponse(mutableListOf(project)))
        viewModel.loadProjects(1)

        assertEquals(CHILD_LIST, viewModel.viewFlipperActiveChild.get())
    }

    private fun createProjectsResponse(data: MutableList<Project>) = ProjectsResponse(data.size, data)

    companion object {
        const val PROJECT_ID = 1
        const val PROJECT_NAME = "Desafio Android"
        const val PROJECT_DESCRIPTION = "Lorem Ipsum Dolor"
        const val PROJECT_STARS = 100
        const val PROJECT_FORKS = 10

        const val OWNER_ID = 1
        const val OWNER_NAME = "Daniel Nazareth"
        const val OWNER_AVATAR = "picture"
    }
}