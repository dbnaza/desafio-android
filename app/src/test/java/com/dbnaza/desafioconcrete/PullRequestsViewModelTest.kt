package com.dbnaza.desafioconcrete

import com.dbnaza.desafioconcrete.models.Models.*
import com.dbnaza.desafioconcrete.network.GitHubService
import com.dbnaza.desafioconcrete.rx.RxSchedulerRule
import com.dbnaza.desafioconcrete.viewmodel.BaseViewModel.Companion.CHILD_LIST
import com.dbnaza.desafioconcrete.viewmodel.BaseViewModel.Companion.CHILD_RELOAD
import com.dbnaza.desafioconcrete.viewmodel.PullRequestsViewModel
import com.google.gson.annotations.SerializedName
import io.mockk.every
import io.mockk.mockk
import io.reactivex.Single
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class PullRequestsViewModelTest {

    private lateinit var owner: Owner
    private lateinit var pullRequest: PullRequest
    private lateinit var gitHubService: GitHubService
    private lateinit var viewModel: PullRequestsViewModel

    @get:Rule
    var rule = RxSchedulerRule()

    @Before
    fun setup() {
        owner = Owner(OWNER_NAME, OWNER_ID, OWNER_AVATAR)
        pullRequest = PullRequest(PULL_REQUEST_ID, PULL_REQUEST_TITLE, owner, PULL_REQUEST_BODY, PULL_REQUEST_DATE)

        gitHubService = mockk(relaxed = true)
        viewModel = PullRequestsViewModel(OWNER_NAME, PROJECT_NAME, gitHubService)
    }

    @Test
    fun whenServiceReturnSuccessShouldShowList() {
        every { gitHubService.pullRequests(any(), any()) } returns Single.just(mutableListOf(pullRequest))

        viewModel.loadPullRequests()

        assertEquals(mutableListOf(pullRequest), viewModel.listResult.get())
    }

    @Test
    fun whenServiceReturnWithErrorAndListIsEmptyShouldShowReloadButton() {
        every { gitHubService.pullRequests(any(), any()) } returns Single.error(Exception())

        viewModel.loadPullRequests()

        assertEquals(CHILD_RELOAD, viewModel.viewFlipperActiveChild.get())
    }

    @Test
    fun whenServiceReturnWithErrorAndListIsNowEmptyShouldShowNotReloadButton() {
        every { gitHubService.pullRequests(any(), any()) } returns Single.error(Exception())

        viewModel.listResult.set(mutableListOf(pullRequest))
        viewModel.loadPullRequests()

        assertEquals(CHILD_LIST, viewModel.viewFlipperActiveChild.get())
    }

    companion object {
        const val PROJECT_NAME = "Desafio Android"

        const val OWNER_ID = 1
        const val OWNER_NAME = "Daniel Nazareth"
        const val OWNER_AVATAR = "picture"

        const val PULL_REQUEST_ID = 1
        const val PULL_REQUEST_TITLE = "Code changes"
        const val PULL_REQUEST_BODY = "Lorem Ipsum Dolor"
        const val PULL_REQUEST_DATE = "2018-07-30T06:56:30Z"
    }
}