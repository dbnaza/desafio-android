package com.dbnaza.desafioconcrete.rx

import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement

class RxSchedulerRule : TestRule {

    private val scheduler = Schedulers.trampoline()

    override fun apply(base: Statement?, description: Description?) = object : Statement() {

        @Throws(Throwable::class)
        override fun evaluate() {
            if (base == null) {
                return
            }

            try {
                RxJavaPlugins.reset()
                RxAndroidPlugins.reset()

                RxJavaPlugins.setIoSchedulerHandler { scheduler }
                RxJavaPlugins.setComputationSchedulerHandler { scheduler }
                RxJavaPlugins.setNewThreadSchedulerHandler { scheduler }

                RxAndroidPlugins.setInitMainThreadSchedulerHandler { scheduler }

                base.evaluate()
            } finally {
                RxJavaPlugins.reset()
                RxAndroidPlugins.reset()
            }
        }
    }
}